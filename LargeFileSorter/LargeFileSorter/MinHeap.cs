﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Large File Sorter
//	File Name:		MinHeap.cs
//	Description:    This program fulfills the necessary functions of a Min Heap.
//	Author:			Brandon Campbell, campbellb1@.etsu.edu
//	Created:		10/20/2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargeFileSorter
{
    /// <summary>
    /// This is the main class that will hold the variables for the Heap itself.
    /// </summary>
    class MinHeap
    {
        public int capacity { get; set; } //Min size of heap
        public int size { get; set; }    //this will be the logical size of the 'heap' concept

        private int position { get; set; }  //this will mark the position of the heap

        public tempFileStruct.tempFile[] heap { get; set; } //this will hold the array that will encompass the heap.


        /// <summary>
        /// Initializes a new instance of the <see cref="MinHeap"/> class.
        /// </summary>
        /// <param name="heapSize">Size of the heap.</param>
        public MinHeap(int heapSize)
        {
            size = 0;   //instantiate the size
            capacity = heapSize + 1;    //instantiate the capacity
            heap = new tempFileStruct.tempFile[capacity];    //instantiate the heap based on capacity
        }



        /// <summary>
        /// Prints this instance.
        /// </summary>
        public void Print()
        {
            //print the 'heap' (not necessarily the array)
            for (int i = 1; i <= size; i++)
            {
                Console.Write(heap[i] + " ");
            }
            Console.WriteLine("\n");
        }

        /// <summary>
        /// Inserts the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Insert(tempFileStruct.tempFile item)
        {
            
            size++; //increase the size

            heap[size] = item;  //put the new input into the last slot of the array.

            //instantiate the parent and current pointers.
            int parent = size;
            int current = size;

            //This loop will rebalance the heap--now that a new value has been added.
            while ((parent /= 2) >= 1)
            {
                if (heap[parent].Value > heap[current].Value)
                {
                    Swap(parent, current);
                    current = parent;
                }
                else
                    return;
            }

        }

        /// <summary>
        /// Swaps the specified index1.
        /// </summary>
        /// <param name="index1">The index1.</param>
        /// <param name="index2">The index2.</param>
        public void Swap(int index1, int index2)
        {

            tempFileStruct.tempFile tempVal = heap[index1]; //hold the temporary value for the switch
            heap[index1] = heap[index2];
            heap[index2] = tempVal;
        }

        /// <summary>
        /// Extracts the minimum from the Min-Heap.
        /// </summary>
        /// <returns></returns>
        public tempFileStruct.tempFile ExtractMin()
        {

            //instantiate pointers we will use to keep track of "where we are"
            int leftChildIndex; //will hold the index of the left child of the current node
            int rightChildIndex;//will hold the index of the right child of the current node
            int MinIndex;       //will hold the current index we are at.


            tempFileStruct.tempFile MinToReturn = heap[1];    //put the Min in a Min variable.
            int valueToGet;                                   //this value is used to hold the integer read in from the read().

            //if we have reached the end of the file...
            if ((valueToGet = heap[1].BinFile.read()) == -1)
            {
                Swap(1, size);//swap the last node with the top
                size--;       //reduce the logical size, we are done with that node and it's corresponding Binary file
            }
            else
            {
                heap[1].Value = (valueToGet); //otherwise, iterate the binaryFile to the next integer by getting the value.
            }

            int siftIndex = 1; //while we sift down the index, use this as a pointer.

            //begin the rebalancing.
            while (siftIndex <= size)
            {
                //set the left and right *potential* children respectively.
                leftChildIndex = siftIndex * 2; 
                rightChildIndex = (siftIndex * 2) + 1;

                //if there is no right child or left child, end it, otherwise set the left child as the index.
                if (rightChildIndex > size)
                {
                    if (leftChildIndex > size)
                        break;
                    else
                        MinIndex = leftChildIndex;
                }
                else //otherwise compare the two to determine which is the larger and set it as the index.
                {
                    if (heap[leftChildIndex].Value > heap[rightChildIndex].Value)
                    {
                        MinIndex = rightChildIndex;
                    }
                    else
                    {
                        MinIndex = leftChildIndex;
                    }

                }

                //compare the parent with the child and if the child is smaller, swap
                if (heap[siftIndex].Value > heap[MinIndex].Value)
                {
                    Swap(siftIndex, MinIndex);
                    siftIndex = MinIndex;
               
                }
                else
                {
                    break;
                }

            }
            
            return MinToReturn; //return the Min.
        }
        /// <summary>
        /// Sorts the heap by simply calling the ExtractMin() until the heap is empty.
        /// </summary>
        public void HeapSort()
        {
            while (size > 0)
            {
                Console.Write(ExtractMin().Value + " ");
            }
        }

        /// <summary>
        /// Prints the array itself--not necessarily the heap.
        /// </summary>
        public void PrintArray()
        {
            for(int i = 1; i < heap.Length; i++)
            {
                Console.WriteLine(heap[i]);
            }
        }

      
    }

    
}
