﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Large File Sorter
//	File Name:		Program.cs
//	Description:    This program takes in large files (that cannot normally be sorted in memory) and sorts them by breaking them up into smaller chunks and then performing a merge sort.
//	Author:			Brandon Campbell, campbellb1@.etsu.edu
//	Created:		11/21/2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using System.Threading;

namespace LargeFileSorter
{
    class Program
    {
        
        
        tempFileStruct mainTempFileStruct = new tempFileStruct(); //Go ahead and instantiate the class that holds our fileStruct.
        [STAThread]
        static void Main(string[] args)
        {
            int numTempFiles = 0;   //this will keep track of the number of temporary files generated from the program.
            int heapSize = 0; //this is the size of heap that will be used to create temp files
            int numFilesToMergePerTime = 0; //this is the amount of files that will be merged at a time.

            //prompt the user for a binary file to sort via an OpenFileDialog
            Console.WriteLine("Please choose a binary file to sort: ");
            Thread.Sleep(800); //allow the user to actually read the prompt before opening the file dialog.
            string filePath = "";   //this will hold the path for the user-selected file.
            OpenFileDialog fbd = new OpenFileDialog();  //this is the file dialog object.
            if (fbd.ShowDialog() == DialogResult.OK)
            {
                filePath = fbd.FileName;
            }
            //prompt the user for how large a heap the user would like to use per initial temp file.
            Console.WriteLine("How large of a heap do you want to use?");
            heapSize = Int32.Parse(Console.ReadLine());

            numTempFiles = CreateTempFiles(filePath, heapSize); //call the CreateTempFiles method to generate the files that will be eventually merged
                                                                //also, store the number of tempfiles that were created.

            //now ask the user how many files they would like to merge per time.
            Console.WriteLine("How many files do you want to merge at a time?");
            numFilesToMergePerTime = Int32.Parse(Console.ReadLine());

            MergeFiles(numFilesToMergePerTime, numTempFiles);//call the MergeFiles method to merge all the tempfiles into an eventual
                                                             //single, sorted file.
        }


        /// <summary>
        /// Creates temporary files that will eventually be merged into a final sorted file in the MergeFiles(). The
        /// tempFiles will be created by creating heaps based on the user-defined size limit. Integers will
        /// be read in from the primary file and tempFiles will be created by printing the Array created by
        /// filling the maxHeap.
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <param name="heapSize">Size of the heap.</param>
        /// <returns></returns>
        public static int CreateTempFiles(string filePath, int heapSize)
        {
            int tempFileCount = 0; //this will be a count of the tempFiles (which will be returned at the end)
            int actualSizeOfHeap = 0;   //this will keep track of what the actual size of the heap is (as opposed to the user-defined size)

            BinaryReader binReader = new BinaryReader(File.Open(filePath, FileMode.Open)); //this will read in from the primary file
            
            //So long as there is more integers left to read within the file, 
            while (binReader.BaseStream.Position != binReader.BaseStream.Length)
            {
                MaxHeap tempHeap = new MaxHeap(heapSize); //this will be the MaxHeap that we will insert the integers in. Eventually becomes the tempFile.
              
                //So long as we have not yet filled the heap AND we have not reached the end of the primary file, keep filling the heap
                while (tempHeap.size < heapSize && binReader.BaseStream.Position < binReader.BaseStream.Length)
                {
                    tempHeap.Insert(binReader.ReadInt32()); //insert the next integer in the file into the heap.
                }
                actualSizeOfHeap = tempHeap.size;   //store the actual size of the current heap
                tempHeap.HeapSort();                //sort the heap to prepare for printing

                tempFileCount++;                    //since we will now create a tempFile, iterate the tempFileCount
                Directory.CreateDirectory("tempfiles");     //create a directory to place the tempFiles in

                //Create the blank tempFile.
                FileStream binStream = new FileStream("tempfiles\\LargeFileSorterTemp_" + tempFileCount, FileMode.Create);  


                BinaryWriter binWriter = new BinaryWriter(binStream);//instantiate a binarywriter to the file we just made
             

                //so long as we have more numbers in our heap, write out the values in each corresponding element
                for (int i = 1; i <= actualSizeOfHeap; i++)
                {
                    binWriter.Write(tempHeap.heap[i]);
                }

                binWriter.Close(); //close the writer
            }
            return tempFileCount; //send back the tempFileCount

        }

        /// <summary>
        /// Merges the tempFiles that were created in the CreateTempFiles() method.
        /// </summary>
        /// <param name="numFilesToMergePerTime">The number files to merge per time.</param>
        /// <param name="numTempFiles">The number of temporary files.</param>
        public static void MergeFiles(int numFilesToMergePerTime, int numTempFiles)
        {
            tempFileStruct.tempFile[] arrayOfStructs = new tempFileStruct.tempFile[numFilesToMergePerTime]; //create our array of the custom tempFile structs
            int nextTempFile = 1;   //this will keep track of what the nextTempFile is that we need to read
            int filesLeft = numTempFiles;   //this will keep track of how many files there are left to be merged
            int createTempFileNumber = numTempFiles + 1;    //this will keep track of what we need to name/number the new tempfiles created in this method
            

            //while there is more than one file left, we need to keep merging...
                while (filesLeft > 1)
                {
                MinHeap mergeMinHeap = new MinHeap(numFilesToMergePerTime); //create a min heap that will allow us to pull out files in order.

                //so long as there are more files to merge in this run and we have more than 0 files, read in the next temp file.
                for (int i = 0; i < numFilesToMergePerTime && filesLeft > 0; i++)
                    {
                        arrayOfStructs[i].BinFile = new BinaryFile("tempfiles\\LargeFileSorterTemp_" + nextTempFile, false);
                        arrayOfStructs[i].Value = arrayOfStructs[i].BinFile.read();
                        filesLeft--;
                        nextTempFile++;
                        mergeMinHeap.Insert(arrayOfStructs[i]);
                    }
                    filesLeft++; //since we will next create a new tempFile from the merged files, iterate the filesLeft
                    
                    FileStream binStream = new FileStream("tempfiles\\LargeFileSorterTemp_" + createTempFileNumber, FileMode.Create);//prepare the new file that we will write by instantiating the FileStream
                    createTempFileNumber++; //the next file we make later will need to be the next number
                    BinaryWriter binWriter = new BinaryWriter(binStream);   //instantiate the BinaryWriter so we can write to the file
                 
                //so long as there are still structs in our heap, extract the minimum value...
                    while (mergeMinHeap.size > 0)
                    {
                        binWriter.Write(mergeMinHeap.ExtractMin().Value);
                    }
                binStream.Close();//close the stream
                binWriter.Close();//close the writer

                //for every file that is within our heap, attempt to close the file (throws many exceptions because some files refuse to be closed)
                for(int i = 0; i < mergeMinHeap.capacity; i++)
                {
                    try
                    {
                        mergeMinHeap.heap[i].BinFile.Close();
                    }
                    catch(Exception e)
                    {
                        //null
                    }
                }
                    
                }

          
                File.Copy("tempfiles\\LargeFileSorterTemp_" + (createTempFileNumber - 1), "SortedFile.bin", true);//create the final sorted file based on the last created tempFile
                BinaryFile finalFile = new BinaryFile("SortedFile.bin", false); //set the finalFile to SortedFile.bin
                Directory.Delete("tempfiles", true);                            //delete our temp folder
                int output; //this is used to hold output values
            string response = "";//this will hold the users response for if they want to display contents

            //ask the user if they want to print to screen all of the sorted integers.
            Console.WriteLine("Do you want to display all of the sorted integers? Y or N (can be overwhelming if file-size is large)");
            response = Console.ReadLine();

            //print values if user hits y or Y, otherwise pass
            switch (response)
            {
                case "Y":
                case "y":
                    while ((output = finalFile.read()) != -1)
                    {
                        Console.Write(output + " ");
                    }
                    break;
                default:
                    break;
            }

            //prompt to exit
            Console.WriteLine("\nPress ENTER to exit");
            Console.ReadLine();

        }
        }
    }



