﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Large File Sorter
//	File Name:		MaxHeap.cs
//	Description:    This program fulfills the necessary functions of a Max Heap.
//	Author:			Brandon Campbell, campbellb1@.etsu.edu
//	Created:		10/20/2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargeFileSorter
{
    /// <summary>
    /// This is the main class that will hold the variables for the Heap itself.
    /// </summary>
    class MaxHeap
    {
        public int capacity { get; set; } //Max size of heap
        public int size { get; set; }    //this will be the logical size of the 'heap' concept

        private int position { get; set; }  //this will mark the position of the heap

        public int[] heap { get; set; } //this will hold the array that will encompass the heap.


        /// <summary>
        /// Initializes a new instance of the <see cref="MaxHeap"/> class.
        /// </summary>
        /// <param name="heapSize">Size of the heap.</param>
        public MaxHeap(int heapSize)
        {
            size = 0;   //instantiate the size
            capacity = heapSize + 1;    //instantiate the capacity
            heap = new int[capacity];    //instantiate the heap based on capacity
        }



        /// <summary>
        /// Prints this instance.
        /// </summary>
        public void Print()
        {
            //print the 'heap' (not necessarily the array)
            for (int i = 1; i <= size; i++)
            {
                Console.Write(heap[i] + " ");
            }
            Console.WriteLine("\n");
        }

        /// <summary>
        /// Inserts the specified item.
        /// </summary>
        /// <param name="item">The item.</param>
        public void Insert(int item)
        {
            
            size++; //increase the size

            heap[size] = item;  //put the new input into the last slot of the array.

            //instantiate the parent and current pointers.
            int parent = size;
            int current = size;

            //This loop will rebalance the heap--now that a new value has been added.
            while ((parent /= 2) >= 1)
            {
                if (heap[parent] < heap[current])
                {
                    Swap(parent, current);
                    current = parent;
                }
                else
                    return;
            }

        }

        /// <summary>
        /// Swaps the specified index1.
        /// </summary>
        /// <param name="index1">The index1.</param>
        /// <param name="index2">The index2.</param>
        public void Swap(int index1, int index2)
        {

            int tempVal = heap[index1]; //this hold the index 1's value until it can be put into index 2
            heap[index1] = heap[index2];
            heap[index2] = tempVal;
        }

        /// <summary>
        /// Extracts the Maximum from the Max-Heap.
        /// </summary>
        /// <returns></returns>
        public int ExtractMax()
        {

            //instantiate pointers we will use to keep track of "where we are"
            int leftChildIndex;
            int rightChildIndex;
            int MaxIndex;

            Swap(1, size); //swap the Max position with the last position to prepare for the extraction.

            int MaxToReturn = heap[size];    //put the Max in a Max variable.

            size--; //reduce the logical size of the heap.

            int siftIndex = 1; //while we sift down the index, use this as a pointer.

            //begin the rebalancing.
            while (siftIndex <= size)
            {
                //set the left and right *potential* children respectively.
                leftChildIndex = siftIndex * 2; 
                rightChildIndex = (siftIndex * 2) + 1;

                //if there is no right child or left child, end it, otherwise set the left child as the index.
                if (rightChildIndex > size)
                {
                    if (leftChildIndex > size)
                        break;
                    else
                        MaxIndex = leftChildIndex;
                }
                else //otherwise compare the two to deterMaxe which is the larger and set it as the index.
                {
                    if (heap[leftChildIndex] < heap[rightChildIndex])
                    {
                        MaxIndex = rightChildIndex;
                    }
                    else
                    {
                        MaxIndex = leftChildIndex;
                    }

                }

                //compare the parent with the child and if the child is smaller, swap
                if (heap[siftIndex] < heap[MaxIndex])
                {
                    Swap(siftIndex, MaxIndex);
                    siftIndex = MaxIndex;
                }
                else
                {
                    break;
                }

            }
            
            return MaxToReturn; //return the Max.
        }
        /// <summary>
        /// Sorts the heap by simply calling the ExtractMax() until the heap is empty.
        /// </summary>
        public void HeapSort()
        {
            while (size > 0)
            {
                ExtractMax();
            }
        }

        /// <summary>
        /// Prints the array itself--not necessarily the heap.
        /// </summary>
        public void PrintArray()
        {
            for(int i = 1; i < heap.Length; i++)
            {
                Console.WriteLine(heap[i]);
            }
        }

      
    }

    
}
