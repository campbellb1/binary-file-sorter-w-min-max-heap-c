﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Large File Sorter
//	File Name:		tempFileStruct.cs
//	Description:    This class houses the tempFile Struct
//	Author:			Brandon Campbell, campbellb1@.etsu.edu
//	Created:		11/27/2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LargeFileSorter
{
    /// <summary>
    /// This class exists to house the struct: tempFile
    /// </summary>
    class tempFileStruct
    {
        /// <summary>
        /// The tempFile struct serves as a structure to hold an integer value and a BinaryFile.
        /// </summary>
        public struct tempFile
        {
            private int value;  //holds an integer value obtained from its housed BinaryFile
            public int Value
            {
                
                get
                {
                    return value;
                }

                set
                {
                    this.value = value;
                }
            }

            private BinaryFile binFile; //the binary file that is housed in the struct. Provides the int value as needed.
            public BinaryFile BinFile
            {
                
                get
                {
                    return binFile;
                }

                set
                {
                    binFile = value;
                }
            }

           

          
        }
    }       
}
