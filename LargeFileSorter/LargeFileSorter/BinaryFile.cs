﻿//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//	Project:		Large File Sorter
//	File Name:		BinaryFile.cs
//	Description:    This serves to fulfill all needed functions related to a Binary file for the Large File Sorter project.
//                  adapted from the "Files Lab" - Algorithms; Dr. Chris Wallace
//	Author:			Brandon Campbell, campbellb1@.etsu.edu
//	Created:		11/28/2016
//
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.InteropServices;

namespace LargeFileSorter
{
    // Class BinaryFile
    //   manages the details of reading and writing fixed
    //   length binary records to from/to a binary file
    class BinaryFile
    {
        public BinaryWriter binOutFile; // Output file
        public BinaryReader binInFile;  // Input file
        public int position = 0;       // Current file position
	    private long length = 0;         // Length of input file
        bool writing = false;            // Writing == true

	    // Constructor
	    // Open fileName for writing if writeit == true,
	    // reading if writeit == false
        public BinaryFile(string fileName, bool writeit)
        {
            writing = writeit;
            if (writing)
            {
                binOutFile = new BinaryWriter(File.Open(fileName, 
		                  FileMode.Create, FileAccess.Write));
            }
            else
            {
                binInFile = new BinaryReader(File.Open(fileName, 
		                FileMode.Open, FileAccess.Read));
		    // Set the length of this file
                length = binInFile.BaseStream.Length;
            } // if
        } // BinaryFile()

	    // Write one binary record
        public void write(byte[] buffer, int size)
        {
            binOutFile.Write(buffer, 0, size);
        } // write()

	    // Read one binary integer record
        public int read()
        {
            if (binInFile.BaseStream.Position == binInFile.BaseStream.Length) //if we have reached the end of the binary file...
            {
                return -1;  //this will serve as a message that the file has ended.
            }
            else
            {
                return binInFile.ReadInt32();   //otherwise read in the next integer
            }
             
            
        } // read()

	    // Return file length
        public long getLength()
        {
            return length;
        } // getLength()

	    // Close whichever file
        public void Close()
        {
            if (writing) binOutFile.Close();
            else binInFile.Close();
        } // Close()

    } // class BinaryFile
}
